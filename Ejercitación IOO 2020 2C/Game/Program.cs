﻿using System;
using System.Collections.Generic;

namespace Game
{
    public class Program
    {
        public static float deltaTime;
        public static DateTime startTime;
        private static float lastFrameTime;

        private static Player player;

        private static void Main(string[] args)
        {
            Initialization();

            while(true)
            {
                InputDetection();
                Update();
                Render();
            }
        }

        private static void Initialization()
        {
            startTime = DateTime.Now;
            Engine.Initialize("Ejemplo", 800, 800);
            player = new Player(50, 50, 0.1f, 0f);
            player.Start();
        }

        private static void InputDetection()
        {
            player.InputDetection();
        }

        private static void Update()
        {
            UpdateTime();
            player.Update();
        }

        private static void Render()
        {
            Engine.Clear();
            Engine.Draw("Textures/Background.png");
            player.Render();
            Engine.Show();
        }

        private static void UpdateTime()
        {
            var currentTime = (float)(DateTime.Now - startTime).TotalSeconds;
            deltaTime = currentTime - lastFrameTime;
            lastFrameTime = currentTime;
        }
    }
}