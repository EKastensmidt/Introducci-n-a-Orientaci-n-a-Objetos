﻿using System.Collections;

namespace Game
{
    public class Player
    {
        public float x;
        public float y;
        public float velocidad = 250;
        private float angle;
        private float scale;
        private Texture texture;

        public Player(float x, float y, float scale, float angle)
        {
            this.x = x;
            this.y = y;
            this.scale = scale;
            this.angle = angle;
        }

        public void Start()
        {
            texture = Engine.GetTexture("Textures/Enemy.png");
        }

        public void InputDetection()
        {

        }

        public void Update()
        {
            Movement();
        }

        public void Render()
        {
            Engine.Draw(texture, x, y, scale, scale, angle, GetOffsetX(), GetOffsetY());
        }

        private float GetOffsetX()
        {
            return (texture.Width * scale) / 2f;
        }

        private float GetOffsetY()
        {
            return (texture.Height * scale) / 2f;
        }

        public void Movement()
        {
            if (Engine.GetKey(Keys.UP))
            {
                y -= velocidad * Program.deltaTime;
            }
            else if (Engine.GetKey(Keys.DOWN))
            {
                y += velocidad * Program.deltaTime;
            }
            else if (Engine.GetKey(Keys.RIGHT))
            {
                x += velocidad * Program.deltaTime;
            }
            else if (Engine.GetKey(Keys.LEFT))
            {
                x -= velocidad * Program.deltaTime;
            }
        }
    }
}

